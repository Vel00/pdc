#include "Lab1.h"

int** make_matrix(int r, int c)
{
	int** M = new int*[r];

	for (int i = 0; i < r; i++)
	{
		M[i] = new int[c]();
	}

	return M;
}

void delete_matrix(int** M, int r)
{
	for (int i = 0; i < r; i++)
	{
		delete[] M[i];
	}

	delete[] M;
}

void random_matrix(int** M, int r, int c)
{
	std::random_device rd;
	std::mt19937 random(rd());

	for (int i = 0; i < r; i++)
	{
		for (int j = 0; j < c; j++)
		{
			M[i][j] = random() % MAX_VALUE;
		}
	}
}

void mul_matrix(int** A, int** B, int** C, int size)
{
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			for (int r = 0; r < size; r++)
			{
				C[i][j] += A[i][r] * B[r][j];
			}
		}
	}
}

int** slice(int** M, int ax, int bx, int ay, int by)
{
	int** A = new int*[bx - ax];

	for (int i = ax, j = 0; i < bx; i++, j++)
	{
		A[j] = M[i] + ay;
	}

	return A;
}

void add_matrix(int** C, int** T, int size)
{
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			C[i][j] += T[i][j];
		}
	}
}

void mul_matrix_parallel(int** A, int** B, int** C, int size)
{
	int** T = make_matrix(size, size);

	int half = size / 2;

	int** A11 = slice(A, 0, half, 0, half);
	int** A12 = slice(A, 0, half, half, size);
	int** A21 = slice(A, half, size, 0, half);
	int** A22 = slice(A, half, size, half, size);

	int** B11 = slice(B, 0, half, 0, half);
	int** B12 = slice(B, 0, half, half, size);
	int** B21 = slice(B, half, size, 0, half);
	int** B22 = slice(B, half, size, half, size);

	int** C11 = slice(C, 0, half, 0, half);
	int** C12 = slice(C, 0, half, half, size);
	int** C21 = slice(C, half, size, 0, half);
	int** C22 = slice(C, half, size, half, size);

	int** T11 = slice(T, 0, half, 0, half);
	int** T12 = slice(T, 0, half, half, size);
	int** T21 = slice(T, half, size, 0, half);
	int** T22 = slice(T, half, size, half, size);

	std::thread threads[8];

	threads[0] = std::thread(mul_matrix, A11, B11, C11, half);
	threads[1] = std::thread(mul_matrix, A11, B12, C12, half);
	threads[2] = std::thread(mul_matrix, A21, B11, C21, half);
	threads[3] = std::thread(mul_matrix, A21, B12, C22, half);
	threads[4] = std::thread(mul_matrix, A12, B21, T11, half);
	threads[5] = std::thread(mul_matrix, A12, B22, T12, half);
	threads[6] = std::thread(mul_matrix, A22, B21, T21, half);
	threads[7] = std::thread(mul_matrix, A22, B22, T22, half);

	for (int i = 0; i < 8; i++)
	{
		threads[i].join();
	}

	threads[0] = std::thread(add_matrix, C11, T11, half);
	threads[1] = std::thread(add_matrix, C12, T12, half);
	threads[2] = std::thread(add_matrix, C21, T21, half);
	threads[3] = std::thread(add_matrix, C22, T22, half);

	for (int i = 0; i < 4; i++)
	{
		threads[i].join();
	}
}


void mul_matrix_parallel_race(int** A, int** B, int** C, int size)
{
	int half = size / 2;

	int** A11 = slice(A, 0, half, 0, half);
	int** A12 = slice(A, 0, half, half, size);
	int** A21 = slice(A, half, size, 0, half);
	int** A22 = slice(A, half, size, half, size);

	int** B11 = slice(B, 0, half, 0, half);
	int** B12 = slice(B, 0, half, half, size);
	int** B21 = slice(B, half, size, 0, half);
	int** B22 = slice(B, half, size, half, size);

	int** C11 = slice(C, 0, half, 0, half);
	int** C12 = slice(C, 0, half, half, size);
	int** C21 = slice(C, half, size, 0, half);
	int** C22 = slice(C, half, size, half, size);

	std::thread threads[8];

	threads[0] = std::thread(mul_matrix, A11, B11, C11, half);
	threads[1] = std::thread(mul_matrix, A11, B12, C12, half);
	threads[2] = std::thread(mul_matrix, A21, B11, C21, half);
	threads[3] = std::thread(mul_matrix, A21, B12, C22, half);
	threads[4] = std::thread(mul_matrix, A12, B21, C11, half);
	threads[5] = std::thread(mul_matrix, A12, B22, C12, half);
	threads[6] = std::thread(mul_matrix, A22, B21, C21, half);
	threads[7] = std::thread(mul_matrix, A22, B22, C22, half);

	for (int i = 0; i < 8; i++)
	{
		threads[i].join();
	}

}



void print_matrix(int** M, int size)
{
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			printf("%d ", M[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}