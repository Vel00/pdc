#pragma once
#include <random>
#include <thread>

constexpr int MAX_VALUE = 100;


int** make_matrix(int r, int c);


void delete_matrix(int** M, int r);


void random_matrix(int** M, int r, int c);


void mul_matrix(int** A, int** B, int** C, int size);


int** slice(int** M, int ax, int bx, int ay, int by);


void add_matrix(int** C, int** T, int size);


void mul_matrix_parallel(int** A, int** B, int** C, int size);


void mul_matrix_parallel_race(int** A, int** B, int** C, int size);


void print_matrix(int** M, int size);