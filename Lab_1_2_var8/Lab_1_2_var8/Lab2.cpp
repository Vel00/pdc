#include <mutex>
#include "Lab2.h"
#include "Lab1.h"


aint** make_atomic_matrix(int r, int c)
{
	aint** M = new aint*[r];

	for (int i = 0; i < r; i++)
	{
		M[i] = new aint[c]();
	}

	return M;
}

void delete_matrix(aint** M, int r)
{
	for (int i = 0; i < r; i++)
	{
		delete[] M[i];
	}

	delete[] M;
}


void mul_matrix(int** A, int** B, aint** C, int size)
{
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			for (int r = 0; r < size; r++)
			{
				C[i][j] += A[i][r] * B[r][j];
			}
		}
	}
}

aint** slice(aint** M, int ax, int bx, int ay, int by)
{
	aint** A = new aint*[bx - ax];

	for (int i = ax, j = 0; i < bx; i++, j++)
	{
		A[j] = M[i] + ay;
	}

	return A;
}


void mul_matrix_parallel_atomic(int** A, int** B, aint** C, int size)
{
	int half = size / 2;

	int** A11 = slice(A, 0, half, 0, half);
	int** A12 = slice(A, 0, half, half, size);
	int** A21 = slice(A, half, size, 0, half);
	int** A22 = slice(A, half, size, half, size);

	int** B11 = slice(B, 0, half, 0, half);
	int** B12 = slice(B, 0, half, half, size);
	int** B21 = slice(B, half, size, 0, half);
	int** B22 = slice(B, half, size, half, size);

	aint** C11 = slice(C, 0, half, 0, half);
	aint** C12 = slice(C, 0, half, half, size);
	aint** C21 = slice(C, half, size, 0, half);
	aint** C22 = slice(C, half, size, half, size);

	std::thread threads[8];

	void(*mul)(int**, int**, aint**, int) = &mul_matrix;

	threads[0] = std::thread(mul, A11, B11, C11, half);
	threads[1] = std::thread(mul, A11, B12, C12, half);
	threads[2] = std::thread(mul, A21, B11, C21, half);
	threads[3] = std::thread(mul, A21, B12, C22, half);
	threads[4] = std::thread(mul, A12, B21, C11, half);
	threads[5] = std::thread(mul, A12, B22, C12, half);
	threads[6] = std::thread(mul, A22, B21, C21, half);
	threads[7] = std::thread(mul, A22, B22, C22, half);

	for (int i = 0; i < 8; i++)
	{
		threads[i].join();
	}
}


std::mutex mutex;

void mul_matrix_critical(int** A, int** B, int** C, int size)
{
	//std::unique_lock<std::timed_mutex> lock(mutex);

	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			int sum = 0;

			for (int r = 0; r < size; r++)
			{
				sum += A[i][r] * B[r][j];
			}

			mutex.lock();

			C[i][j] += sum;

			mutex.unlock();
		}
	}
}


void mul_matrix_parallel_critical(int** A, int** B, int** C, int size)
{
	int half = size / 2;

	int** A11 = slice(A, 0, half, 0, half);
	int** A12 = slice(A, 0, half, half, size);
	int** A21 = slice(A, half, size, 0, half);
	int** A22 = slice(A, half, size, half, size);

	int** B11 = slice(B, 0, half, 0, half);
	int** B12 = slice(B, 0, half, half, size);
	int** B21 = slice(B, half, size, 0, half);
	int** B22 = slice(B, half, size, half, size);

	int** C11 = slice(C, 0, half, 0, half);
	int** C12 = slice(C, 0, half, half, size);
	int** C21 = slice(C, half, size, 0, half);
	int** C22 = slice(C, half, size, half, size);

	std::thread threads[8];

	void(*mul)(int**, int**, int**, int) = &mul_matrix_critical;

	threads[0] = std::thread(mul, A11, B11, C11, half);
	threads[1] = std::thread(mul, A11, B12, C12, half);
	threads[2] = std::thread(mul, A21, B11, C21, half);
	threads[3] = std::thread(mul, A21, B12, C22, half);
	threads[4] = std::thread(mul, A12, B21, C11, half);
	threads[5] = std::thread(mul, A12, B22, C12, half);
	threads[6] = std::thread(mul, A22, B21, C21, half);
	threads[7] = std::thread(mul, A22, B22, C22, half);

	for (int i = 0; i < 8; i++)
	{
		threads[i].join();
	}
}


bool equals(int**A, aint**B, int size)
{
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			if (A[i][j] != B[i][j])
				return false;
		}
	}
	return true;
}


bool equals(int**A, int**B, int size)
{
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			if (A[i][j] != B[i][j])
				return false;
		}
	}

	return true;
}