#pragma once
#include <atomic>

using aint = std::atomic<int>;


aint** make_atomic_matrix(int r, int c);


void delete_matrix(aint** M, int r);


void mul_matrix(int** A, int** B, aint** C, int size);


void mul_matrix_critical(int** A, int** B, int** C, int size);


aint** slice(aint** M, int ax, int bx, int ay, int by);


void mul_matrix_parallel_atomic(int** A, int** B, aint** C, int size);


void mul_matrix_parallel_critical(int** A, int** B, int** C, int size);


bool equals(int**A, aint**B, int size);


bool equals(int**A, int**B, int size);

