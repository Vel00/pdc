#include "Lab3.h"
#include <omp.h>

void mul_matrix_omp(int** A, int** B, int** C, int size)
{
	#pragma omp parallel for
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			for (int r = 0; r < size; r++)
			{
				C[i][j] += A[i][r] * B[r][j];
			}
		}
	}
}
