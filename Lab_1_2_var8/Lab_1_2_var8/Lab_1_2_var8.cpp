﻿#include <stdio.h>
#include <chrono>
#include <omp.h>
#include "Lab1.h"
#include "Lab2.h"
#include "Lab3.h"


int main()
{
	using std::chrono::high_resolution_clock;
	using std::chrono::duration_cast;
	using std::chrono::milliseconds;

	int size = 768;

	int** A = make_matrix(size, size);
	random_matrix(A, size, size);

	int** B = make_matrix(size, size);
	random_matrix(B, size, size);

	int** sequential = make_matrix(size, size);
	int** race = make_matrix(size, size);
	int** parallel = make_matrix(size, size);
	aint** atomic = make_atomic_matrix(size, size);
	int** critical = make_matrix(size, size);
	int** omp_ = make_matrix(size, size);
	


	auto start = high_resolution_clock::now();
	mul_matrix(A, B, sequential, size);
	auto end = high_resolution_clock::now();
	auto duration = duration_cast<milliseconds>(end - start);
	printf("Sequential: %llu\n", duration.count());


	start = high_resolution_clock::now();
	mul_matrix_parallel_race(A, B, race, size);
	end = high_resolution_clock::now();
	duration = duration_cast<milliseconds>(end - start);
	printf("Race: %llu ", duration.count());

	if (equals(sequential, race, size))
		printf("Matrices are equal\n");
	else
		printf("Matrices are not equal\n");


	start = high_resolution_clock::now();
	mul_matrix_parallel(A, B, parallel, size);
	end = high_resolution_clock::now();
	duration = duration_cast<milliseconds>(end - start);
	printf("Parallel: %llu ", duration.count());

	if (equals(sequential, parallel, size))
		printf("Matrices are equal\n");
	else
		printf("Matrices are not equal\n");


	start = high_resolution_clock::now();
	mul_matrix_parallel_atomic(A, B, atomic, size);
	end = high_resolution_clock::now();
	duration = duration_cast<milliseconds>(end - start);
	printf("Atomic: %llu ", duration.count());


	if (equals(sequential, atomic, size))
		printf("Matrices are equal\n");
	else
		printf("Matrices are not equal\n");


	start = high_resolution_clock::now();
	mul_matrix_parallel_critical(A, B, critical, size);
	end = high_resolution_clock::now();
	duration = duration_cast<milliseconds>(end - start);
	printf("Critical: %llu ", duration.count());

	if (equals(sequential, critical, size))
		printf("Matrices are equal\n");
	else
		printf("Matrices are not equal\n");

	omp_set_num_threads(4);
	start = high_resolution_clock::now();
	mul_matrix_omp(A, B, omp_, size);
	end = high_resolution_clock::now();
	duration = duration_cast<milliseconds>(end - start);
	printf("OpenMP: %llu ", duration.count());

	if (equals(sequential, omp_, size))
		printf("Matrices are equal\n");
	else
		printf("Matrices are not equal\n");

	delete_matrix(A, size);
	delete_matrix(B, size); 
	delete_matrix(sequential, size);
	delete_matrix(parallel, size);
	delete_matrix(atomic, size);	
	delete_matrix(critical, size);
	delete_matrix(omp_, size);

}

